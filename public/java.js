function onClick() {
    let a = document.getElementsByName("price");
    let b = document.getElementsByName("kolvo");
    let r = document.getElementById("result");
    let q = parseInt(a[0].value * b[0].value);
    if (!q || q <=0) {
        r.innerHTML = "Error";
        alert("Введите корректное значение");
    }
    else {
        r.innerHTML = q;
        return false;
    }
}

window.addEventListener("DOMContentLoaded", function (event) {
    console.log("DOM fully loaded and parsed");
    let b = document.getElementById("button");
    b.addEventListener("click", onClick);
});
